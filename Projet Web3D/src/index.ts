import { Clock, Color, WebGLRenderer } from 'three';
import {MainView} from "./MainView";
import {update} from "@tweenjs/tween.js";

/**
 * Renderer Initialization.
 */

const canvas = document.getElementById('main-canvas') as HTMLCanvasElement;
const renderer = new WebGLRenderer({ canvas, antialias: true });
renderer.setClearColor(new Color('#464646'));

const clock = new Clock();
const mainView = new MainView(renderer);
mainView.initialize()
mainView.resize(canvas.width, canvas.height)
window.onmousemove = mainView.onMouseMove.bind(mainView);
window.onmousedown = mainView.onMouseDown.bind(mainView)

function buttonFunction() {
  i++
  if (i == 2)
  {
    i = 0
    mainView.switchCamera(!mainView._isPerspectiv)
    console.log("switch")
  }

}

var params = {
  ClickMe: buttonFunction
};

let i : number = 0

var folder = mainView._gui.addFolder('SwitchCam');

folder.add(params, 'ClickMe');
folder.open();

/**
 * Lifecycle: Update & Render.
 */

function animate() {
  window.requestAnimationFrame(animate);
  update()
  mainView.update(clock.getDelta(), clock.getElapsedTime());
  mainView.render();
}

animate();


const resizeObserver = new ResizeObserver(entries => {
  if (entries.length > 0) {
    const entry = entries[0];
    canvas.width = window.devicePixelRatio * entry.contentRect.width;
    canvas.height = window.devicePixelRatio * entry.contentRect.height;
    renderer.setSize(canvas.width, canvas.height, false);
    mainView.resize(canvas.width, canvas.height);
  }
});

resizeObserver.observe(canvas);
