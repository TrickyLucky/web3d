import { GUI } from 'dat.gui';
import {
    Scene,
    WebGLRenderer,
    Vector3,
    Vector2,
    OrthographicCamera,
    Raycaster,
    Event,
    SpotLight,
    DirectionalLight,
    PerspectiveCamera,
    Camera,
    PointLight,
    AmbientLight,
    Layers
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {Room} from "./Room";
import {Chunk} from "./Chunk";
import {EffectComposer} from "three/examples/jsm/postprocessing/EffectComposer";
import {SAOPass} from "three/examples/jsm/postprocessing/SAOPass";
import {RenderPass} from "three/examples/jsm/postprocessing/RenderPass";
import {Easing, Tween} from "@tweenjs/tween.js";
import {OutlinePass} from "three/examples/jsm/postprocessing/OutlinePass";

interface GUIProperties {
    perspectivCamFov: number;
    perspectivCamPosX: number;
    perspectivCamPosY: number;
    perspectivCamPosZ: number;
    perspectivCamRotX: number;
    orthographicCamPosX: number;
    orthographicCamPosY: number;
    orthographicCamPosZ: number;
    spotlightPosX: number;
    spotlightPosY: number;
    spotlightPosZ: number;
    spotlightTargetPosX: number;
    spotlightTargetPosY: number;
    spotlightTargetPosZ: number;
}

export class MainView {

    protected _renderer: WebGLRenderer;
    protected _scene: Scene;

    protected _properties: GUIProperties;

    private _perpectivCam : PerspectiveCamera
    protected _orthographicCam: OrthographicCamera;
    private _activeCamera : Camera;
    public _isPerspectiv : boolean;

    private _orthographicOrbitControls: OrbitControls;
    private _perpectivOrbitControls : OrbitControls;

    public _gui: GUI;
    private _roomRaycaster: Raycaster

    private _pointLight: PointLight;
    private _spotLight1: SpotLight;
    private _spotLight2: SpotLight;
    private _spotLight3: SpotLight;
    private _spotLight4: SpotLight;
    private _spotLight5: SpotLight;
    private _spotLight6: SpotLight;
    private _spotLight7: SpotLight;
    private _spotLight8: SpotLight;
    private _spotLightB1: SpotLight;
    private _spotLightB2: SpotLight;
    private _spotLightB3: SpotLight;
    private _spotLightB4: SpotLight;
    private _ambiant: AmbientLight;
    private _dir: DirectionalLight;

    private NorthChunk: Chunk
    private NorthEastChunk: Chunk
    private NorthWestChunk: Chunk
    private SouthChunk: Chunk
    private SouthEastChunk: Chunk
    private SouthWestChunk: Chunk
    private WestChunk: Chunk
    private EastChunk: Chunk
    private CenterChunk : Chunk
    protected  _composer: EffectComposer;
    private _outline: OutlinePass
    private _currentRoom: Room;

    private _mouse: Vector2 = new Vector2();

    private autoRotateTimer: number;
    private autoRotateThreshold: number = 5

    public static colors: string[] = [
            '131417',
            '374047',
            'acb4b9',
            '7C6862',
            'A3AB84',
            'D6CCB1',
            'F8D5C4',
            'A3AE99',
            'EFF2F2',
            'B0C5C1',
            '8B8C8C',
            '565F59',
            'CB304A',
            'FED7C8',
            'C7BDBD',
            '3DCBBE',
            '389389',
            '85BEAE',
            'F2DABA',
            'F2A97F',
            'D85F52',
            'D92E37',
            'FC9736',
            'F7BD69',
            'A4D09C',
            '4C8A67',
            '25608A',
            '75C8C6',
            'F5E4B7',
            'FFFFFF',
            'E69041',
            'E56013',
            '11101D',
            '630609',
            'C9240E',
            'EC4B17',
            '281A1C',
            '4F556F',
            '64739B',
            'CDBAC7',
            '946F43',
            '66533C',
            '173A2F',
            '153944',
            '27548D',
            '438AAC'
    ]
    private _tray = document.getElementById('js-tray-slide');

    constructor(renderer: WebGLRenderer) {
        this._renderer = renderer;
        this._scene = new Scene();

        this._properties = {
            perspectivCamFov : 30,
            perspectivCamPosX : 4,
            perspectivCamPosY : 5,
            perspectivCamPosZ : -4,
            perspectivCamRotX : 0,
            orthographicCamPosX : 4,
            orthographicCamPosY : 5,
            orthographicCamPosZ : -4,
            spotlightPosX : 0,
            spotlightPosY : 0,
            spotlightPosZ : 0,
            spotlightTargetPosX: 0,
            spotlightTargetPosY: 0,
            spotlightTargetPosZ: 0
        };

        const frustumHeight = 3;
        var aspect = window.innerWidth / window.innerHeight;
        this._orthographicCam = new OrthographicCamera(- frustumHeight * aspect / 2, frustumHeight * aspect / 2, frustumHeight / 2, - frustumHeight / 2, 1, 2000 );
        this._orthographicCam.position.set(4, 5, -4);
        this._orthographicCam.lookAt (new Vector3(0,0,0));

        this._perpectivCam = new PerspectiveCamera(33)
        this._perpectivCam.position.set(4, 5, -4);
        this._perpectivCam.scale.set(1, 1, 1)
        this._perpectivCam.lookAt (new Vector3(-10,-10,-10));

        this._activeCamera = this._orthographicCam
        this._isPerspectiv = false

        this._gui = new GUI();
        this._gui.hide();

        this._roomRaycaster = new Raycaster()
        this._roomRaycaster.layers.set( 1 );

        this._orthographicOrbitControls = new OrbitControls( this._orthographicCam, renderer.domElement );
        this._orthographicOrbitControls.enablePan = true;
        this._orthographicOrbitControls.enableRotate = false;
        this._orthographicOrbitControls.enableZoom = false;

        this._perpectivOrbitControls = new OrbitControls(this._perpectivCam, renderer.domElement)
        this._perpectivOrbitControls.maxDistance = 100;
        this._perpectivOrbitControls.minPolarAngle = 0;
        this._perpectivOrbitControls.maxPolarAngle =  Math.PI * 0.5;
        this._perpectivOrbitControls.enableDamping = true;
        this._perpectivOrbitControls.dampingFactor = .05;
        this._perpectivOrbitControls.autoRotate = false
        //this._perpectivOrbitControls.minZoom = .15;
        this._perpectivOrbitControls.enablePan = false;
        this._perpectivOrbitControls.minZoom = .15;


        this.generateFirstChunks()
    }


    public initialize() {
       // this._gui.show();
        this._renderer.autoClear = false;

        this._gui.add(this._properties, 'perspectivCamFov', 0, 180, 0.05);
        this._gui.add(this._properties, 'perspectivCamPosX', -100, 100, 0.05);
        this._gui.add(this._properties, 'perspectivCamPosY', -100, 100, 0.05);
        this._gui.add(this._properties, 'perspectivCamPosZ', -100, 100, 0.05);
        this._gui.add(this._properties, 'perspectivCamRotX', -5, 5, 0.005);
        this._gui.add(this._properties, 'orthographicCamPosX', -100, 100, 0.05);
        this._gui.add(this._properties, 'orthographicCamPosY', -100, 100, 0.05);
        this._gui.add(this._properties, 'orthographicCamPosZ', -100, 100, 0.05);
        this._gui.add(this._properties, 'spotlightPosX', -10, 10, .05);
        this._gui.add(this._properties, 'spotlightPosY', -10, 10, .05);
        this._gui.add(this._properties, 'spotlightPosZ', -10, 10, .05);
        this._gui.add(this._properties, 'spotlightTargetPosX', -10, 10, .05);
        this._gui.add(this._properties, 'spotlightTargetPosY', -10, 10, .05);
        this._gui.add(this._properties, 'spotlightTargetPosZ', -10, 10, .05);

        this.buildColors(MainView.colors);

        this.initComposer(this._activeCamera)

        //const ambientLight = new AmbientLight( 0xcccccc, 0.4);

        //this._scene.add( ambientLight );
        this._dir = new DirectionalLight;
        this._dir.intensity = .25
        this._dir.position.copy(this._activeCamera.position);
        this._dir.target.position.set(0 , 0 , 0);
        this._scene.add(this._dir);

        this._pointLight = new PointLight;
        //this._spotLight.position.copy(this._activeCamera.position);
        //this._activeCamera.add(this._spotLight);
        //this._activeCamera.add(this._spotLight.target);

        this._pointLight.intensity = 2.25
        this._pointLight.decay = .25
        this._pointLight.distance = 4

        // /this._scene.add(this._activeCamera);

        this._pointLight.rotation.copy(this._activeCamera.rotation)
        this._pointLight.position.copy(this._activeCamera.position);
        this._scene.add(this._pointLight);

        this._spotLight1 = new SpotLight()
        this._spotLight1.intensity = 1;
        this._spotLight1.angle = .25;
        this._spotLight1.penumbra = .5;
        this._spotLight1.visible = false;
        this._scene.add(this._spotLight1);
        this._scene.add(this._spotLight1.target)
        this._spotLight2 = new SpotLight()
        this._spotLight2.intensity = 1;
        this._spotLight2.angle = .25;
        this._spotLight2.penumbra = .5;
        this._spotLight2.visible = false;
        this._scene.add(this._spotLight2);
        this._scene.add(this._spotLight2.target)
        this._spotLight3 = new SpotLight()
        this._spotLight3.intensity = 1;
        this._spotLight3.angle = .25;
        this._spotLight3.penumbra = .5;
        this._spotLight3.visible = false;
        this._scene.add(this._spotLight3);
        this._scene.add(this._spotLight3.target)
        this._spotLight4 = new SpotLight()
        this._spotLight4.intensity = 1;
        this._spotLight4.angle = .25;
        this._spotLight4.penumbra = .5;
        this._spotLight4.visible = false;
        this._scene.add(this._spotLight4);
        this._scene.add(this._spotLight4.target)

        this._spotLight5 = new SpotLight()
        this._spotLight5.intensity = .5;
        this._spotLight5.angle = .75;
        this._spotLight5.penumbra = .3;
        this._spotLight5.visible = false;
        this._scene.add(this._spotLight5);
        this._scene.add(this._spotLight5.target)
        this._spotLight6 = new SpotLight()
        this._spotLight6.intensity = .5;
        this._spotLight6.angle = .75;
        this._spotLight6.penumbra = .3;
        this._spotLight6.visible = false;
        this._scene.add(this._spotLight6);
        this._scene.add(this._spotLight6.target)
        this._spotLight7 = new SpotLight()
        this._spotLight7.intensity = .5;
        this._spotLight7.angle = .75;
        this._spotLight7.penumbra = .3;
        this._spotLight7.visible = false;
        this._scene.add(this._spotLight7);
        this._scene.add(this._spotLight7.target)
        this._spotLight8 = new SpotLight()
        this._spotLight8.intensity = .5;
        this._spotLight8.angle = .75;
        this._spotLight8.penumbra = .3;
        this._spotLight8.visible = false;
        this._scene.add(this._spotLight8);
        this._scene.add(this._spotLight8.target)

        this._spotLightB1 = new SpotLight()
        this._spotLightB1.intensity = .75;
        this._spotLightB1.angle = .5;
        this._spotLightB1.penumbra = .5;
        this._spotLightB1.visible = false;
        this._scene.add(this._spotLightB1);
        this._scene.add(this._spotLightB1.target)
        this._spotLightB2 = new SpotLight()
        this._spotLightB2.intensity = .75;
        this._spotLightB2.angle = .5;
        this._spotLightB2.penumbra = .5;
        this._spotLightB2.visible = false;
        this._scene.add(this._spotLightB2);
        this._scene.add(this._spotLightB2.target)
        this._spotLightB3 = new SpotLight()
        this._spotLightB3.intensity = .75;
        this._spotLightB3.angle = .5;
        this._spotLightB3.penumbra = .5;
        this._spotLightB3.visible = false;
        this._scene.add(this._spotLightB3);
        this._scene.add(this._spotLightB3.target)
        this._spotLightB4 = new SpotLight()
        this._spotLightB4.intensity = .75;
        this._spotLightB4.angle = .5;
        this._spotLightB4.penumbra = .5;
        this._spotLightB4.visible = false;
        this._scene.add(this._spotLightB4);
        this._scene.add(this._spotLightB4.target)

        this._ambiant = new AmbientLight()
        this._ambiant.intensity = .1;
        this._ambiant.visible = false;
        this._scene.add(this._ambiant);

        //const light2 = new PointLight();
        //light2.position.set(0, 5, 0)
        //this._scene.add(light2);
    }

    private initComposer(cam : Camera)
    {
        this._composer = new EffectComposer( this._renderer );
        this._composer.setSize( window.innerWidth || 1, window.innerHeight || 1)
        //var ssaaRenderPass = new SSAARenderPass(this._scene, cam, 0xAAAAAA, 0)
        //saaRenderPass.setSize(window.innerWidth || 1, window.innerHeight || 1)
        //this._composer.addPass(ssaaRenderPass)
        var renderPass = new RenderPass( this._scene, cam );
        this._composer.addPass( renderPass );
        var saoPass = new SAOPass( this._scene, cam, false, true,  new Vector2(1024,1024) );
        saoPass.resolution.set(128, 128);
        saoPass.params = {
            output: 0,
            saoBias: 0,
            saoIntensity: .005,
            saoScale: 20,
            saoKernelRadius: 50,
            saoMinResolution: 0,
            saoBlur: true,
            saoBlurRadius: 8,
            saoBlurStdDev: 6,
            saoBlurDepthCutoff: 0.01
        }
        this._composer.addPass( saoPass );
        this._outline = new OutlinePass( new Vector2(256,256), this._scene, this._activeCamera)
        this._outline.edgeStrength = 10
        this._outline.edgeThickness = .1
        this._outline.edgeGlow = .2
        this._composer.addPass(this._outline);
    }

    public async generateFirstChunks() {
        this.CenterChunk = await this.generateChunk(0, 0)
        this.WestChunk = await this.generateChunk(-Chunk.chunkSize, 0)
        this.EastChunk = await this.generateChunk(Chunk.chunkSize, 0)
        this.NorthChunk = await this.generateChunk( 0, Chunk.chunkSize)
        this.SouthChunk = await this.generateChunk( 0, -Chunk.chunkSize)
        this.NorthEastChunk = await this.generateChunk( Chunk.chunkSize, Chunk.chunkSize)
        this.NorthWestChunk = await this.generateChunk( -Chunk.chunkSize, Chunk.chunkSize)
        this.SouthEastChunk = await this.generateChunk( Chunk.chunkSize, -Chunk.chunkSize)
        this.SouthWestChunk = await this.generateChunk( -Chunk.chunkSize, -Chunk.chunkSize)
    }

    public async generateChunk(x : number, z : number, ) {
        const newChunk = new Chunk(x, z)
        await newChunk.InitRooms()
        this._scene.add(newChunk)
        return newChunk;
    }

    public async updateRoomGrid() {

        let  camCoordinates = new Vector2()

        if (this._isPerspectiv) {
            const parentChunk = <Chunk><unknown>this._currentRoom.parent
            camCoordinates = parentChunk.coordinates
        }
        else
        {
            this._roomRaycaster.setFromCamera( new Vector2(0, 0), this._activeCamera);

            // calculate objects intersecting the picking ray
            const intersects = this._roomRaycaster.intersectObjects( this._scene.children )
            if (intersects.length) {
                // pick the first object. It's the closest one
                const pickedObject = intersects[0].object;
                const parentRoom = <Room><unknown>pickedObject.parent
                const parentChunk = <Chunk><unknown>parentRoom.parent
                camCoordinates = parentChunk.coordinates
            }
        }

        if (camCoordinates.x > this.CenterChunk.coordinates.x) {
            console.log("traveled east")
            //console.log(this.CenterChunk.coordinates.x)
            this.WestChunk.destroy()
            this.NorthWestChunk.destroy()
            this.SouthWestChunk.destroy()
            this.WestChunk = this.CenterChunk
            this.NorthWestChunk = this.NorthChunk
            this.SouthWestChunk = this.SouthChunk

            this.NorthChunk = this.NorthEastChunk
            this.CenterChunk = this.EastChunk
            this.SouthChunk = this.SouthEastChunk

            this.NorthEastChunk = await this.generateChunk(this.NorthChunk.coordinates.x + Chunk.chunkSize, this.NorthChunk.coordinates.y)
            this.SouthEastChunk = await this.generateChunk(this.SouthChunk.coordinates.x + Chunk.chunkSize, this.SouthChunk.coordinates.y)
            this.EastChunk = await this.generateChunk(this.CenterChunk.coordinates.x + Chunk.chunkSize, this.CenterChunk.coordinates.y)
            return
        }

        if (camCoordinates.x < this.CenterChunk.coordinates.x) {
            console.log("traveled west")
            //console.log(this.CenterChunk.coordinates.x)
            this.EastChunk.destroy()
            this.NorthEastChunk.destroy()
            this.SouthEastChunk.destroy()
            this.EastChunk = this.CenterChunk
            this.NorthEastChunk = this.NorthChunk
            this.SouthEastChunk = this.SouthChunk

            this.NorthChunk = this.NorthWestChunk
            this.CenterChunk = this.WestChunk
            this.SouthChunk = this.SouthWestChunk

            this.NorthWestChunk = await this.generateChunk(this.NorthChunk.coordinates.x - Chunk.chunkSize, this.NorthChunk.coordinates.y)
            this.SouthWestChunk = await this.generateChunk(this.SouthChunk.coordinates.x - Chunk.chunkSize, this.SouthChunk.coordinates.y)
            this.WestChunk = await this.generateChunk(this.CenterChunk.coordinates.x - Chunk.chunkSize, this.CenterChunk.coordinates.y)
            return;
        }

        if (camCoordinates.y > this.CenterChunk.coordinates.y) {
            console.log("traveled north")
            this.SouthWestChunk.destroy()
            this.SouthChunk.destroy()
            this.SouthEastChunk.destroy()
            this.SouthWestChunk = this.WestChunk
            this.SouthChunk = this.CenterChunk
            this.SouthEastChunk = this.EastChunk

            this.EastChunk = this.NorthEastChunk
            this.CenterChunk = this.NorthChunk
            this.WestChunk = this.NorthWestChunk

            this.NorthWestChunk = await this.generateChunk(this.WestChunk.coordinates.x, this.WestChunk.coordinates.y + Chunk.chunkSize)
            this.NorthChunk = await this.generateChunk(this.CenterChunk.coordinates.x, this.CenterChunk.coordinates.y  + Chunk.chunkSize)
            this.NorthEastChunk = await this.generateChunk(this.EastChunk.coordinates.x, this.EastChunk.coordinates.y  + Chunk.chunkSize)
            return;
        }

        if (camCoordinates.y < this.CenterChunk.coordinates.y) {
            console.log("traveled south")
            this.NorthWestChunk.destroy()
            this.NorthChunk.destroy()
            this.NorthEastChunk.destroy()
            this.NorthWestChunk = this.WestChunk
            this.NorthChunk = this.CenterChunk
            this.NorthEastChunk = this.EastChunk

            this.EastChunk = this.SouthEastChunk
            this.CenterChunk = this.SouthChunk
            this.WestChunk = this.SouthWestChunk

            this.SouthWestChunk = await this.generateChunk(this.WestChunk.coordinates.x, this.WestChunk.coordinates.y - Chunk.chunkSize)
            this.SouthChunk = await this.generateChunk(this.CenterChunk.coordinates.x, this.CenterChunk.coordinates.y  - Chunk.chunkSize)
            this.SouthEastChunk = await this.generateChunk(this.EastChunk.coordinates.x, this.EastChunk.coordinates.y  - Chunk.chunkSize)
        }
    }



    public switchCamera(perpectiv : boolean) {

        if (perpectiv) {
            console.log("Swtiching to Perspective");
            this._isPerspectiv = true;
            this._activeCamera = this._perpectivCam;
            this._pointLight.visible = false;
            this._spotLight1.visible = true;
            this._spotLight2.visible = true;
            this._spotLight3.visible = true;
            this._spotLight4.visible = true;
            this._spotLight5.visible = true;
            this._spotLight6.visible = true;
            this._spotLight7.visible = true;
            this._spotLight8.visible = true;
            this._spotLightB1.visible = true;
            this._spotLightB2.visible = true;
            this._spotLightB3.visible = true;
            this._spotLightB4.visible = true;
            this._ambiant.visible = true;
            this._dir.visible = false;
            document.querySelectorAll(".tray__swatch").forEach((x)=> x.style.display = 'block')
        } else {
            console.log("Swtiching to Ortographic");
            this._isPerspectiv = false;
            this._activeCamera = this._orthographicCam;
            this._pointLight.visible = true;
            this._spotLight1.visible = false;
            this._spotLight2.visible = false;
            this._spotLight3.visible = false;
            this._spotLight4.visible = false;
            this._spotLight5.visible = false;
            this._spotLight6.visible = false;
            this._spotLight7.visible = false;
            this._spotLight8.visible = false;
            this._spotLightB1.visible = false;
            this._spotLightB2.visible = false;
            this._spotLightB3.visible = false;
            this._spotLightB4.visible = false;
            this._ambiant.visible = false;
            this._dir.visible = true
            if (this._currentRoom) {
                this._currentRoom.remove(this._currentRoom._textSprite)
                this._currentRoom.remove(this._currentRoom._buttonMesh)
            }
            document.querySelectorAll(".tray__swatch").forEach((x)=> x.style.display = 'none')
        }

        this.initComposer(this._activeCamera)
    }

    public buildColors(colors: string[]) {
        for (let [i, color] of colors.entries()) {
            let swatch = document.createElement('div');
            swatch.classList.add('tray__swatch');

            swatch.style.background = "#" + color;

            swatch.setAttribute('data-key', String(i));
            this._tray.append(swatch);
            const swatches = document.querySelectorAll(".tray__swatch");

            for (const swatch of swatches) {
                swatch.addEventListener('click', this.selectSwatch);
            }
            document.querySelectorAll(".tray__swatch").forEach((x)=> x.style.display = 'none')
        }
    }

    // @ts-ignore
    public selectSwatch = e => {
        if (this._currentRoom == undefined)
            return
        var a =  e.currentTarget.dataset.key;
        let color = MainView.colors[parseInt(a)];
        this._currentRoom._model.traverse( ( object ) => {
            if (object.isMesh) {
                object.material.color.set( "#" + color);
            }
        });
    };

    public onWindowResize() {
        const width = window.innerWidth || 1;
        const height = window.innerHeight || 1;
        //this._cam.aspect = window.innerWidth / window.innerHeight;
        //this._renderer.setSize(window.innerWidth, window.innerHeight);
        this._composer.setSize( width, height );
    }

    public destroy() {
        this._gui.hide();
    }

    public onMouseMove(event : Event) {
        this.autoRotateTimer = 0
        this._perpectivOrbitControls.autoRotate = false
        
        this._mouse.x = ( event.clientX /  window.innerWidth ) * 2 - 1;
        this._mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
        let roomRaycaster2 = new Raycaster();

        const layer2 = new Layers()
        layer2.set(2)

        if (!this._isPerspectiv) {
            roomRaycaster2.layers.set(1)
            roomRaycaster2.setFromCamera(this._mouse, this._orthographicCam);
            const intersects = roomRaycaster2.intersectObject(this._scene);
            if (intersects && intersects.length > 0) {
                const pickedObject = intersects[0].object;
                const parentRoom = <Room><unknown>pickedObject.parent
                if (parentRoom && parentRoom._model)
                    this._outline.selectedObjects = parentRoom._model.children;
            }
        }
        else {
            roomRaycaster2.layers.enable(2)
            roomRaycaster2.setFromCamera(this._mouse, this._perpectivCam);
            const intersects = roomRaycaster2.intersectObject(this._scene);
            if (intersects && intersects.length > 0 && (intersects[0].object.layers.mask & layer2.mask) > 0) {
                const pickedObject = intersects[0].object;
                const parentRoom = <Room><unknown>pickedObject.parent?.parent
                if (parentRoom && parentRoom != this._currentRoom && parentRoom._model)
                    this._outline.selectedObjects = parentRoom._model.children;
                if (parentRoom && parentRoom == this._currentRoom)
                    this._outline.selectedObjects = [];
            }
            else {
                this._outline.selectedObjects = [];
            }
        }
    }

    public onMouseDown(event : Event) {
        this.autoRotateTimer = 0
        this._perpectivOrbitControls.autoRotate = false

        if (event.button != 0)
            return

        let roomRaycaster = new Raycaster();

        const layer2 = new Layers()
        layer2.set(2)

        const layer3 = new Layers()
        layer3.set(3)

        if (this._isPerspectiv) {
            roomRaycaster.setFromCamera(this._mouse, this._perpectivCam);
            roomRaycaster.layers.enable(2)
            roomRaycaster.layers.enable(3)
            const intersects = roomRaycaster.intersectObject(this._scene);
            if (intersects && intersects.length > 0) {
                if ((intersects[0].object.layers.mask & layer2.mask) > 0)
                {
                    const pickedObject = intersects[0].object;
                    const parentRoom = <Room><unknown>pickedObject.parent?.parent
                    if (parentRoom != null && this._currentRoom != parentRoom) {
                        this._currentRoom.remove(this._currentRoom._textSprite)
                        this._currentRoom.remove(this._currentRoom._buttonMesh)
                        this.changeRoom(parentRoom, true)
                    }
                }

                if ((intersects[0].object.layers.mask & layer3.mask) > 0)
                {
                    this.switchCamera(false)
                }
            }
        } else {
            roomRaycaster.setFromCamera(this._mouse, this._orthographicCam);
            roomRaycaster.layers.set(1)
            const intersects = roomRaycaster.intersectObject(this._scene);
            if (intersects && intersects.length > 0) {
                const pickedObject = intersects[0].object;
                const parentRoom = <Room><unknown>pickedObject.parent
                this.changeRoom(parentRoom, false)
            }
        }
    }


    public changeRoom(parentRoom: Room, fromRoom: boolean)
    {
        this._currentRoom = parentRoom
        const roomPos = new Vector3()
        parentRoom.getWorldPosition(roomPos)
        parentRoom.generateText(new Vector3(.75, .32, 1 - .051), "Description:", parentRoom._descriptionText, true)
        parentRoom.generateBackButton()
        //this._perpectivCam.lookAt(parentRoom.position.x, parentRoom.position.y, parentRoom.position.z)
        // this._spotLight.target.position.set(roomPos.x, 0 , roomPos.z);
        if (!fromRoom) {
            this.switchCamera(true)
        }

        this._perpectivOrbitControls.target.set(roomPos.x + 0.5, roomPos.y + 0.2, roomPos.z + 0.5)

        this._ambiant.position.set(roomPos.x +.5, roomPos.y + .5, roomPos.z + .5)
        this._spotLight1.position.set(roomPos.x , roomPos.y + 1.1, roomPos.z )
        this._spotLight1.target.position.set(roomPos.x + .5 , roomPos.y - .1, roomPos.z + .5)
        this._spotLight2.position.set(roomPos.x + 1, roomPos.y + 1.1, roomPos.z + 1)
        this._spotLight2.target.position.set(roomPos.x + .5 , roomPos.y - .1, roomPos.z + .5)
        this._spotLight3.position.set(roomPos.x + 1, roomPos.y + 1.1, roomPos.z + 0)
        this._spotLight3.target.position.set(roomPos.x + .5 , roomPos.y - .1, roomPos.z + .5)
        this._spotLight4.position.set(roomPos.x + 0, roomPos.y + 1.1, roomPos.z + 1)
        this._spotLight4.target.position.set(roomPos.x + .5 , roomPos.y - .1, roomPos.z + .5)

        this._spotLight5.position.set(roomPos.x + .1, roomPos.y + 0, roomPos.z + .1)
        this._spotLight5.target.position.set(roomPos.x + 0 , roomPos.y + 1, roomPos.z + 0)
        this._spotLight6.position.set(roomPos.x + .9, roomPos.y + 0, roomPos.z + .9)
        this._spotLight6.target.position.set(roomPos.x + 1 , roomPos.y + 1, roomPos.z + 1)
        this._spotLight7.position.set(roomPos.x + .9, roomPos.y + 0, roomPos.z + .1)
        this._spotLight7.target.position.set(roomPos.x + 1 , roomPos.y + 1, roomPos.z + 0)
        this._spotLight8.position.set(roomPos.x + .1, roomPos.y + 0, roomPos.z + .9)
        this._spotLight8.target.position.set(roomPos.x + 0 , roomPos.y + 1, roomPos.z + 1)

        this._spotLightB1.position.set(roomPos.x + 1.5, roomPos.y + 1.1, roomPos.z + .5)
        this._spotLightB1.target.position.set(roomPos.x +1.5 , roomPos.y - .1, roomPos.z + .5)

        this._spotLightB2.position.set(roomPos.x - .5, roomPos.y + 1.1, roomPos.z + .5)
        this._spotLightB2.target.position.set(roomPos.x - .5 , roomPos.y - .1, roomPos.z + .5)

        this._spotLightB3.position.set(roomPos.x + .5, roomPos.y + 1.1, roomPos.z + 1.5)
        this._spotLightB3.target.position.set(roomPos.x + .5 , roomPos.y - .1, roomPos.z + 1.5)

        this._spotLightB4.position.set(roomPos.x + .5, roomPos.y + 1.1, roomPos.z - .5)
        this._spotLightB4.target.position.set(roomPos.x + .5 , roomPos.y - .1, roomPos.z - .5)

        this.travelCamera(new Vector3(roomPos.x + 0.15, 0.2, roomPos.z + 0.15), this._perpectivCam, 3000, fromRoom)
        console.log("switched")
    }


    private travelCamera(endPos: Vector3, cam: Camera, duration: number, fromRoom: boolean) {
        const startPos = cam.position

        if (fromRoom) {
            new Tween(startPos).to(endPos,500).easing(Easing.Quadratic.Out).start();
        }
        else
        {
            this._perpectivOrbitControls.maxDistance = 100;
            new Tween(startPos).to(endPos, duration).easing(Easing.Quadratic.Out)
                .onStart(() => {
                    const fovprop = {fov: 40}
                    new Tween(fovprop).to({fov: 90},duration).onUpdate(()=> {
                        this._perpectivCam.fov = fovprop.fov
                        this._perpectivCam.updateProjectionMatrix()
                    }).start();}).start().onComplete(()=>{
                this._perpectivOrbitControls.maxDistance = 0.5;
            })
        }
    }

    private lerp(v0: number, v1: number, amt: number, maxMove = 0, minDiff = 0.0001) {
        let diff = v1 - v0;
        if (maxMove > 0) {
            diff = Math.min(diff, maxMove);
            diff = Math.max(diff, -maxMove);
        }
        if (Math.abs(diff) < minDiff) {
            return v1;
        }
        return v0 + diff * amt;
    };


    public update(delta: number, elapsed: number) {
        //this.updateCamera()
        this.autoRotateTimer+= delta
        if (this.autoRotateTimer > this.autoRotateThreshold)
            this._perpectivOrbitControls.autoRotate = true

        this._orthographicOrbitControls.update()
        this._perpectivOrbitControls.update()
        this.updateRoomGrid()
        this._pointLight.position.set(this._activeCamera.position.x * 1.25 - 3.5, this._activeCamera.position.y - 2, this._activeCamera.position.z * 1.25 + 4.5);
        this._perpectivCam.updateProjectionMatrix()
    }

    public render() {
        this._composer.render();
    }

    public resize(w: number, h: number) {
        const aspect = w / h;
        this._perpectivCam.aspect = aspect;
        this._perpectivCam.updateProjectionMatrix();
        this._orthographicCam.left = - 3 * aspect / 2
        this._orthographicCam.right = 3 * aspect / 2
        this._orthographicCam.updateProjectionMatrix()
    }
}