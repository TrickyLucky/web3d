import {
    BoxGeometry, BufferGeometry,
    Group,
    Material,
    Mesh,
    MeshBasicMaterial,
    MeshStandardMaterial,
    Object3D,
    PlaneBufferGeometry,
    PlaneGeometry, PointLight,
    Texture,
    Vector2,
    Vector3
} from "three";
import {GLTF} from "three/examples/jsm/loaders/GLTFLoader";

import {LoaderManager} from "./LoaderManager";

export class Room extends Object3D {
    public _model : Group;
    private _roomMeshs : Mesh[]
    private _roomGeometries : BufferGeometry[]
    public _roomMaterials : Material[]

    public _buttonMesh: Mesh
    private _price: string
    public _descriptionText: string
    public static roomSize : number = 1;
    public coordinates : Vector2;
    public _textSprite: Mesh;
    public _furnitures: string[] = ['assets/models/woodencrate.glb',
        'assets/models/scarf.glb',
        'assets/models/musa.glb',
        'assets/models/chair.gltf',
        'assets/models/moonchair.glb',
        'assets/models/plant.glb',
        'assets/models/sofa.glb',
        ]

    public _descriptions: string[] = ['Cette boite de stockage vinta-ge permettra de donner un     look industriel à vos interieurs',
        "Cette écharpe-rideaux fera    palir d'envie vos voisins",
        'Cette sculpture de  qualité à prix compétitif égaiera votre intér-  ieur',
        'Cette chaise comfortable et   premium donnera du cachet à   votre salon',
        'Ce lot de chaise sera parfait dans une chambre d\'ado',
        'Cette plante habiera votre interieur',
        'Ce canapé de standing allie   parfaitement comfort et esthét-isme']

    public _prices: string[]= ['112€ TTC',
        '67.99€ TTC',
        '159.99€ TTC',
        '220€ TTC',
        '39.99€ TTC',
        '49.99€ TTC',
        '359.99 TTC']
    constructor(x: number, y: number, description: string) {
        super();
        this.position.set(x, 0, y)
        this.coordinates = new Vector2(x, y)
         this._descriptionText = description
    }

    public makeTextSprite(title:string, message:string, withPrice: boolean) {
        var parameters = {};
        var fontface = parameters.fontface || 'Helvetica';
        var fontsize = parameters.fontsize || 250;
        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');
        canvas.width = 4000;
        canvas.height = 4000;

        context.font = 315 + "px " + fontface;
        //context.lineWidth = 10000;
        context.textAlign = "start"
        context.textBaseline = "alphabetic"
        // get size data (height depends only on font size)
        var metrics = context.measureText(message);
        var textWidth = 30;

        // text color
        context.fillStyle = 'rgba(0, 0, 0, 0)';
        context.fillRect(0, 0, context.canvas.width, context.canvas.height);
        context.fillStyle = 'white';
        let positionY = 250
        context.fillText(title, 10, positionY, metrics.width);
        context.font = fontsize + "px " + fontface;

        positionY += 400
        let i = 0
        while (i*textWidth < message.length) {
            context.fillText(message.substring(textWidth * i, textWidth * (i + 1)), 10, positionY + 300 * i, metrics.width);
            i++
        }

        if (withPrice) {
            context.font = 315 + "px " + fontface;
            context.fillText('Prix:', 10, positionY + 300 * (i + 1) + 200, metrics.width);
            context.font = fontsize + "px " + fontface;
            context.fillText(this._price, 10, positionY + 300 * (i + 2) + 200, metrics.width);
        }

        // canvas contents will be used for a texture
        var texture = new Texture(canvas)
        texture.needsUpdate = true;

        var spriteMaterial = new MeshBasicMaterial({ map: texture, transparent: true });
        var geometry = new PlaneBufferGeometry(1, 1.25);
        var sprite = new Mesh(geometry, spriteMaterial)
        this.add( sprite);
        geometry.dispose()
        spriteMaterial.dispose()
        return sprite;
    }


    public async generateText(pos: Vector3, title: string, text: string, withPrice: boolean) {
        this._textSprite = this.makeTextSprite(title,  text, withPrice);
        this._textSprite.position.copy(pos);
        this._textSprite.scale.set(.25,.25,.25)
        this._textSprite.rotateY(Math.PI)
    }



    public async generateFurniture() {
        const index = Math.floor(Math.random() * this._furnitures.length)
        const furniture = this._furnitures[index]
        this._descriptionText = this._descriptions[index]
        this._price = this._prices[index]
        await LoaderManager.getInstance().loadGLTF(furniture).then((gltf: GLTF) => {
            this._model = gltf.scene.clone()
            this._model.traverse( function( object ) {

                object.layers.enable( 2 );

            } );
            this.add(this._model)
            switch (furniture) {
                case 'assets/models/woodencrate.glb':
                    this._model.scale.set(.1, .1, .1)
                    this._model.position.set(0.5, 0.15, 0.5)
                    return
                case 'assets/models/chair.gltf':
                    this._model.scale.set(0.3, 0.35, 0.3)
                    this._model.position.set(0.5, 0, 0.5)
                    return;
                case 'assets/models/scarf.glb':
                    this._model.scale.set(0.5, .5, .5)
                    this._model.position.set(-1.7, 0.05, 3.6)
                    return;
                case 'assets/models/musa.glb':
                    this._model.scale.set(0.001, 0.001, 0.001)
                    this._model.position.set(0.5, 0.05, 0.5)
                    return;
                case 'assets/models/moonchair.glb':
                    this._model.scale.set(0.003, 0.003, 0.003)
                    this._model.position.set(0.5, 0.05, 0.5)
                    return;
                case 'assets/models/plant.glb':
                    this._model.scale.set(0.1, 0.1, 0.1)
                    this._model.position.set(0.5, 0.05, 0.5)
                    return;
                case 'assets/models/sofa.glb':
                    this._model.scale.set(0.002, 0.002, 0.002)
                    this._model.position.set(0.7, 0.05, 0.5)
                    return;
            }
        });
    }

    public async generateWalls() {
        this._roomMeshs = []
        this._roomGeometries = []
        this._roomMaterials = []

        const floorGeometry = new BoxGeometry(  Room.roomSize, 0.1, Room.roomSize);
        this._roomGeometries.push(floorGeometry)
        const floorMaterial = new MeshStandardMaterial();

        await LoaderManager.getInstance().loadTexture('assets/textures/wood_plank_light_clear_basecolor.png').then((texture: Texture) => {
            floorMaterial.map = texture;
            floorMaterial.needsUpdate = true;
        });
        await LoaderManager.getInstance().loadTexture('assets/textures/wood_plank_light_clear_metallic.png').then((texture: Texture) => {
            floorMaterial.metalnessMap = texture;
            floorMaterial.needsUpdate = true;
        });
        await LoaderManager.getInstance().loadTexture('assets/textures/wood_plank_light_clear_normal.png').then((texture: Texture) => {
            floorMaterial.normalMap = texture;
            floorMaterial.needsUpdate = true;
        });
        await LoaderManager.getInstance().loadTexture('assets/textures/wood_plank_light_clear_roughness.png').then((texture: Texture) => {
            floorMaterial.roughnessMap = texture;
            floorMaterial.needsUpdate = true;
        });

        this._roomMaterials.push(floorMaterial)
        const floorMesh = new Mesh( floorGeometry, floorMaterial );
        floorMesh.position.set( Room.roomSize / 2, 0,  Room.roomSize / 2)
        floorMesh.layers.enable(1)
        this.add(floorMesh)
        this._roomMeshs.push(floorMesh)

        const wallMaterial = new MeshStandardMaterial();
        /*await LoaderManager.getInstance().loadTexture('assets/textures/concrete_painted_AlbedoTransparency.png').then((texture: Texture) => {
            wallMaterial.map = texture;
            wallMaterial.needsUpdate = true;
        });*/
        await LoaderManager.getInstance().loadTexture('assets/textures/concrete_painted_MetallicSmoothness.png').then((texture: Texture) => {
            wallMaterial.metalnessMap = texture;
            wallMaterial.needsUpdate = true;
        });
        await LoaderManager.getInstance().loadTexture('assets/textures/concrete_painted_Normal.png').then((texture: Texture) => {
            wallMaterial.normalMap = texture;
            wallMaterial.normalScale = new Vector2(.5, .5)
            wallMaterial.needsUpdate = true;
        });

        this._roomMaterials.push(wallMaterial)

        const wallFirstGeometry = new BoxGeometry( 0.05, 1, 0.5);

        this._roomGeometries.push(wallFirstGeometry)

        const wall1Mesh = new Mesh( wallFirstGeometry, wallMaterial );
        wall1Mesh.position.set(0.025, 0, 0.15)
        //wall1Mesh.layers.set(0)
        this.add(wall1Mesh)
        this._roomMeshs.push(wall1Mesh)

        const wall2Mesh = new Mesh( wallFirstGeometry, wallMaterial );
        wall2Mesh.position.set(0.025, 0, 0.85)
        //wall2Mesh.layers.set(0)
        this.add(wall2Mesh)
        this._roomMeshs.push(wall2Mesh)

        const wall3Mesh = new Mesh( wallFirstGeometry, wallMaterial );
        wall3Mesh.position.set(0.975, 0, 0.15)
        //wall3Mesh.layers.set(0)
        this.add(wall3Mesh)
        this._roomMeshs.push(wall3Mesh)

        const wall4Mesh = new Mesh( wallFirstGeometry, wallMaterial );
        wall4Mesh.position.set(0.975, 0, 0.85)
        //wall4Mesh.layers.set(0)
        this.add(wall4Mesh)
        this._roomMeshs.push(wall4Mesh)

        const wallSecondGeometry = new BoxGeometry( 0.5, 1, 0.05);
        this._roomGeometries.push(wallSecondGeometry)

        const wall5Mesh = new Mesh( wallSecondGeometry, wallMaterial );
        wall5Mesh.position.set(0.15, 0, 0.025)
        //wall5Mesh.layers.set(0)
        this.add(wall5Mesh)
        this._roomMeshs.push(wall5Mesh)

        const wall6Mesh = new Mesh( wallSecondGeometry, wallMaterial );
        wall6Mesh.position.set(0.85, 0, 0.025)
        //wall6Mesh.layers.set(0)
        this.add(wall6Mesh)
        this._roomMeshs.push(wall6Mesh)

        const wall7Mesh = new Mesh( wallSecondGeometry, wallMaterial );
        wall7Mesh.position.set(0.15, 0, 0.975)
        //wall7Mesh.layers.set(0)
        this.add(wall7Mesh)
        this._roomMeshs.push(wall7Mesh)

        const wall8Mesh = new Mesh( wallSecondGeometry, wallMaterial );
        wall8Mesh.position.set(0.85, 0, 0.975)
        //wall8Mesh.layers.set(0)
        this.add(wall8Mesh)
        this._roomMeshs.push(wall8Mesh)

        const roofMaterial = new MeshStandardMaterial();
        const roofGeometry = new PlaneGeometry(1, 1)
        this._roomGeometries.push(roofGeometry)
        const roofMesh = new Mesh(roofGeometry, roofMaterial);
        this.add(roofMesh)
        roofMesh.position.copy(new Vector3(0,.5,0))
        roofMesh.rotateX(Math.PI/2)
        this._roomMaterials.push(roofMaterial)
        this._roomMeshs.push(roofMesh)


    }

    public generateBackButton()
    {
        const backButtonMaterial = new MeshStandardMaterial({color: '#B10F2E', emissive: '#B10F2E', emissiveIntensity: .5});
        const backButtonGeometry = new BoxGeometry(0.02, 0.02, 0.02)

        const backButtonMesh = new Mesh(backButtonGeometry, backButtonMaterial);
        backButtonMesh.layers.enable(3)
        this.add(backButtonMesh)
        var point = new PointLight('#B10F2E', 3, .04)
        backButtonMesh.add(point)
        point.position.copy(new Vector3(0,0,-.001))
        backButtonMesh.position.set(0.33,.46,0.95)
        backButtonMaterial.dispose()
        backButtonGeometry.dispose()
        this._buttonMesh = backButtonMesh
        this.generateText(new Vector3(.18, .345, 1 - .051), "", "Retour à la vue d'ensemble", false)
    }

    public destroy() {
        this._roomMaterials.forEach(m => m.dispose())
        this._roomGeometries.forEach(g => g.dispose())
        this._roomMeshs.forEach(m => this.remove(m))
        this.remove(this._model)
    }
}