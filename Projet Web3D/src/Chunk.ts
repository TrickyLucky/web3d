import {Object3D, Vector2} from "three";
import {Room} from "./Room";

export class Chunk extends Object3D{

    public static chunkSize : number = 4;
    public coordinates : Vector2;
    public rooms: Room[]

    constructor(x : number, y : number) {
        super()
        this.coordinates = new Vector2(x, y)
        this.position.set(x, 0, y)
        console.log(this.position)
        this.rooms = []
    }

    public async InitRooms() {
        for (var i = -Chunk.chunkSize / 2; i < Chunk.chunkSize / 2; i++) {
            for (var j = -Chunk.chunkSize / 2; j < Chunk.chunkSize / 2; j++) {
                //console.log("constructing room")
                await this.ContructRoom(i, j)
            }
        }
    }

    public async ContructRoom(x: number, y:number) {
        const newRoom = new Room(x, y, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sollicitudin hendrerit iaculis. Cras a massa hendrerit, condimentum ante quis, scelerisque enim. Etiam diam sem, dapibus et blandit quis, lobortis finibus arcu. Ut ultrices neque condimentum, blandit metus nec, cursus lectus. Quisque a facilisis sem, sed rhoncus nunc. Maecenas iaculis molestie odio, in viverra justo feugiat eget. Nullam dictum urna ut lorem elementum, ut congue nisi hendrerit. Sed a dolor arcu. Duis efficitur posuere dolor vitae consectetur. Duis velit ex, efficitur id euismod sit amet, varius consequat orci. Aliquam mattis eros et nisi ullamcorper, ac lobortis leo luctus. Sed sagittis mollis porttitor. Vivamus id mi luctus, lobortis leo at, gravida augue.")
        await newRoom.generateFurniture()
        await newRoom.generateWalls()
        this.add( newRoom);
        this.rooms.push(newRoom)
        return newRoom;
    }

    public destroy() {
        this.rooms.forEach(room => room.destroy())
    }
}