import {Texture, TextureLoader} from "three";
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {DRACOLoader} from "three/examples/jsm/loaders/DRACOLoader";

export class LoaderManager {

    private static instance: LoaderManager;

    private _textureLoader : TextureLoader
    private _gltfLoader : GLTFLoader

    private _loadedTextures : Map<string, Texture> = new Map<string, Texture>()
    private _loadedGLTF : Map<string, GLTF> = new Map<string, GLTF>()

    constructor() {
        this._textureLoader = new TextureLoader()
        this._gltfLoader = new GLTFLoader()
        const dracoLoader = new DRACOLoader();
        dracoLoader.setDecoderConfig({ type: 'js' });
        dracoLoader.setDecoderPath( 'node_modules/three/examples/js/libs/draco/gltf/' );
        this._gltfLoader.setDRACOLoader( dracoLoader );
    }

    public static getInstance(): LoaderManager {
        if (!LoaderManager.instance) {
            LoaderManager.instance = new LoaderManager();
        }

        return LoaderManager.instance;
    }

    public async loadTexture(url: string): Promise<Texture> {
        if (this._loadedTextures.has(url))
        {
            //console.log("found texture")
            return new Promise((res, rej) => res(this._loadedTextures.get(url)))
        }

        console.log("loading texture")
        const res = await this._textureLoader.loadAsync(url).then((texture: Texture) => {
            this._loadedTextures.set(url, texture)
            console.log("loaded texture")
            return texture
        });
        return res;
    }

    public async loadGLTF(url: string): Promise<GLTF> {
        if (this._loadedGLTF.has(url))
        {
            //console.log("found gltf")
            return new Promise<GLTF>((res, rej) => res(this._loadedGLTF.get(url)))
        }

        console.log("loading gltf")
        const res = await this._gltfLoader.loadAsync(url).then((gltf: GLTF) => {
            this._loadedGLTF.set(url, gltf)
            console.log("loaded gltf")
            return gltf
        });
        return res;
    }
}